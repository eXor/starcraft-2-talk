package starcraft_2_talk;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author Albert Groothedde <albertgroothedde@gmail.com>
 */
public class Helper {
    // # Started Completed Supply Object Minerals Gas Energy

    String[][] data;
    Timer timer = new Timer();

    public DefaultTableModel btnGetBuildOrder(String url) {
        
	//change url to 'api' url
	url = url.replace("http://haploid.nl/sc2/build_order/?buildOrder=", "http://haploid.nl/sc2/build_order/update.php?buildOrder=");
	//System.out.println("New url: " + url);
	//this.rawOutput = getURL(url);
	String table = getTable(url);

	int cols = 8;
	String[] tds = table.split("\n");
	String[] colNames = new String[cols];

	for (int i = 0; i < cols; i++) {
	    colNames[i] = tds[i];
	}

	int rows = (tds.length + 1 - 8) / cols;

	String[][] data = new String[rows][cols];

	int counter = 8;
	for (int row = 0; row < rows; row++) {
	    for (int col = 0; col < cols; col++) {
	        //System.out.println("data[" + row + "][" + col + "] plek " + counter + " : " + tds[counter]);
	        data[row][col] = tds[counter];
	        counter++;
	    }
	}

	this.data = data;

	return new DefaultTableModel(data, colNames);
    }

    private String getTable(String rawUrl) {

        String table = new String();

        try {
	URL url = new URL(rawUrl);
	HTMLEditorKit kit = new HTMLEditorKit();
	HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
	doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
	Reader HTMLReader = new InputStreamReader(url.openConnection().getInputStream());
	kit.read(HTMLReader, doc, 0);

	//  Get an iterator for all HTML tags.
	ElementIterator it = new ElementIterator(doc);
	Element elem;

	while (it.next() != null) {
	    elem = it.current();
	    //System.out.println(elem.getName());
	    if (elem.getName() == "table") {
	        table = doc.getText(elem.getStartOffset(), elem.getEndOffset() - elem.getStartOffset());
	        break; // get first table only
	    }
	}
        } catch (Exception e) {
	System.out.println(e.toString());
	return null;
        }
        return table;
    }

    public String[][] getData() {
        return data;
    }
}
