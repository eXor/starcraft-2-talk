/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package starcraft_2_talk;

import java.io.BufferedInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author Albert Groothedde <albertgroothedde@gmail.com>
 */
public class GameCounter extends Thread {

    private final String[][] data;
    private String gameTime;
    private int currentRow = 0;
    private int gameSeconds = 0;
    GUI gui;
    Map<String, String> sounds = new HashMap<>();

    public GameCounter(GUI gui, int skipSeconds, int skipRow, String[][] data) {
        this.gameSeconds = skipSeconds;
        this.currentRow = skipRow;
        this.data = data;

        this.sounds = new HashMap<>();
        sounds.put("Probe", "WarpProbe.wav");
        sounds.put("Pylon", "WarpPylon.wav");
        sounds.put("Gateway", "WarpGateway.wav");
        sounds.put("Zealot", "WarpZealot.wav");
        sounds.put("CB: Zealot", "ChronoGateway.wav");
        sounds.put("CB: Probe", "ChronoNexus.wav");
        sounds.put("Send scout", "ProbeScout.wav");
        sounds.put("Assimilator", "WarpAssimilator.wav");
        sounds.put("Transfer 3 workers to gas", "Probes3Gas.wav");
        sounds.put("Stalker", "WarpStalker.wav");
        sounds.put("Transform to Warpgate", "TransformWarpgate.wav");
        sounds.put("Nexus", "WarpNexus.wav");
        sounds.put("Forge", "WarpForge.wav");
        sounds.put("Cybernetics Core", "WarpCybCore.wav");
        sounds.put("Twilight Council", "WarpTwilightCouncil.wav");
        
    }

    public void run() {
        while (this.currentRow < this.data.length) {

	this.gameTime = getSecondsInTime(gameSeconds);
	System.out.println(this.gameTime);
	while (data[this.currentRow][1].equals(this.gameTime)) {
	    System.out.println("Row " + this.currentRow + " is " + data[this.currentRow][4]);
	    notify(data[this.currentRow][4]);
	    this.currentRow++;

	    if (this.currentRow == this.data.length) {
	        break;
	    }
	}
	this.gameSeconds++;
	try {
	    Thread.sleep(950);
	} catch (Exception e) {
	    System.out.println(e.toString());
	}
        }
        System.out.println("No more build orders to process.");
    }

    private String getSecondsInTime(int totalSeconds) {
        int minutes = 0;
        int tenths = 0;
        while (totalSeconds - 60 >= 0) {
	minutes++;
	totalSeconds = totalSeconds - 60;
        }
        while (totalSeconds - 10 >= 0) {
	tenths++;
	totalSeconds = totalSeconds - 10;
        }
        String seconds = "";
        if (tenths < 1) {
	seconds = "0" + totalSeconds;
        } else {
	seconds = "" + (tenths * 10 + totalSeconds);
        }

        return "" + minutes + ":" + seconds;
    }

    private int getLastTimeSeconds() {
        String lastGameTime = data[data.length - 1][2];
        String[] lastTime = lastGameTime.split(":");
        int seconds = Integer.parseInt(lastTime[0]) * 60 + Integer.parseInt(lastTime[1]);

        return seconds;
    }

    public void notify(String action) {
        try {
	play(this.sounds.get(action));
        } catch (Exception e) {
	play("NotAvailable.wav");
        }
    }

    public void play(String filename) {
        int playTimes = 0;
        try {
	try (Clip clip = AudioSystem.getClip()) {
	    clip.open(AudioSystem.getAudioInputStream(
		new BufferedInputStream(getClass().getResourceAsStream("/Sounds/" + filename))));
	    clip.start();
	    while (!clip.isRunning()) {
	        Thread.sleep(100);
	        playTimes++;
	    }
	    while (clip.isRunning()) {
	        Thread.sleep(100);
	        playTimes++;
	    }
	    clip.stop();
	}
        } catch (Exception e) {
	e.printStackTrace();
        }
        this.gameSeconds = this.gameSeconds + (playTimes * 100 / 1000);
    }

    public HashMap<String, Object> getAllInfo() {
        HashMap<String, Object> info = new HashMap<>();

        info.put("timecurrent", this.gameTime);
        info.put("timeleft", this.getSecondsInTime(this.getLastTimeSeconds() - this.gameSeconds));
        info.put("timetotal", this.getSecondsInTime(this.getLastTimeSeconds()));

        info.put("max", getLastTimeSeconds());
        info.put("value", this.gameSeconds);

        if (this.currentRow - 1 >= 0) {
	info.put("last", "" + data[this.currentRow - 1][4]);
        } else {
	info.put("last", "No Last");
        }
        info.put("next", "" + data[this.currentRow + 1][4]);

        info.put("selected", this.currentRow);

        return info;
    }
}
